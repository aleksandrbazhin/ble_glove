#include "sdk_common.h"
//#if NRF_MODULE_ENABLED(glove_service_client)

#include "glove_service_client.h"
#include "ble_db_discovery.h"
#include "ble_types.h"
#include "ble_srv_common.h"
#include "ble_gattc.h"
//#define NRF_LOG_MODULE_NAME glove_service_client
#include "nrf_log.h"
//NRF_LOG_MODULE_REGISTER();

#define TX_BUFFER_MASK         0x07                  /**< TX Buffer mask, must be a mask of continuous zeroes, followed by continuous sequence of ones: 000...111. */
#define TX_BUFFER_SIZE         (TX_BUFFER_MASK + 1)  /**< Size of send buffer, which is 1 higher than the mask. */

#define WRITE_MESSAGE_LENGTH   BLE_CCCD_VALUE_LEN    /**< Length of the write message for CCCD. */
#define WRITE_MESSAGE_LENGTH   BLE_CCCD_VALUE_LEN    /**< Length of the write message for CCCD. */

typedef enum
{
    READ_REQ,  /**< Type identifying that this tx_message is a read request. */
    WRITE_REQ  /**< Type identifying that this tx_message is a write request. */
} tx_request_t;

/**@brief Structure for writing a message to the peer, i.e. CCCD.
 */
typedef struct
{
    uint8_t                  gattc_value[WRITE_MESSAGE_LENGTH];  /**< The message to write. */
    ble_gattc_write_params_t gattc_params;                       /**< GATTC parameters for this message. */
} write_params_t;

/**@brief Structure for holding data to be transmitted to the connected central.
 */
typedef struct
{
    uint16_t     conn_handle;  /**< Connection handle to be used when transmitting this message. */
    tx_request_t type;         /**< Type of this message, i.e. read or write message. */
    union
    {
        uint16_t       read_handle;  /**< Read request message. */
        write_params_t write_req;    /**< Write request message. */
    } req;
} tx_message_t;


static tx_message_t m_tx_buffer[TX_BUFFER_SIZE];  /**< Transmit buffer for messages to be transmitted to the central. */
static uint32_t     m_tx_insert_index = 0;        /**< Current index in the transmit buffer where the next message should be inserted. */
static uint32_t     m_tx_index = 0;               /**< Current index in the transmit buffer from where the next message to be transmitted resides. */


/**@brief Function for passing any pending request from the buffer to the stack.
 */
static void tx_buffer_process(void)
{
    if (m_tx_index != m_tx_insert_index)
    {
        uint32_t err_code;

        if (m_tx_buffer[m_tx_index].type == READ_REQ)
        {
            err_code = sd_ble_gattc_read(m_tx_buffer[m_tx_index].conn_handle,
                                         m_tx_buffer[m_tx_index].req.read_handle,
                                         0);
        }
        else
        {
            err_code = sd_ble_gattc_write(m_tx_buffer[m_tx_index].conn_handle,
                                          &m_tx_buffer[m_tx_index].req.write_req.gattc_params);
        }
        if (err_code == NRF_SUCCESS)
        {
            NRF_LOG_DEBUG("SD Read/Write API returns Success..");
            m_tx_index++;
            m_tx_index &= TX_BUFFER_MASK;
        }
        else
        {
            NRF_LOG_DEBUG("SD Read/Write API returns error. This message sending will be "
                "attempted again..");
        }
    }
}


/**@brief Function for handling write response events.
 *
 * @param[in] p_glove_service_c Pointer to the glove Client structure.
 * @param[in] p_ble_evt   Pointer to the BLE event received.
 */
static void on_write_rsp(glove_service_client_t *p_glove_service_c, ble_evt_t const *p_ble_evt)
{
    // Check if the event if on the link for this instance
    if (p_glove_service_c->conn_handle != p_ble_evt->evt.gattc_evt.conn_handle)
    {
        return;
    }
    // Check if there is any message to be sent across to the peer and send it.
    tx_buffer_process();
}


/**@brief Function for handling Handle Value Notification received from the SoftDevice.
 *
 * @details This function will uses the Handle Value Notification received from the SoftDevice
 *          and checks if it is a notification of Button state from the peer. If
 *          it is, this function will decode the state of the sensors and send it to the
 *          application.
 *
 * @param[in] p_glove_service_c Pointer to the Glove Client structure.
 * @param[in] p_ble_evt   Pointer to the BLE event received.
 */
static void on_hvx(glove_service_client_t *p_glove_service_c, ble_evt_t const *p_ble_evt)
{
    // Check if the event is on the link for this instance
    if (p_glove_service_c->conn_handle != p_ble_evt->evt.gattc_evt.conn_handle)
    {
        return;
    }
    // Check if this is a Button notification.
    if (p_ble_evt->evt.gattc_evt.params.hvx.handle == p_glove_service_c->peer_glove_service_db.sensor_handle)
    {
        if (p_ble_evt->evt.gattc_evt.params.hvx.len == sizeof(sensor_data_t))
        {
            glove_service_client_evt_t glove_service_client_evt;

            glove_service_client_evt.evt_type                  = BLE_GLOVE_SERVICE_CLIENT_EVT_SENSOR_NOTIFICATION;
            glove_service_client_evt.conn_handle               = p_glove_service_c->conn_handle;
            
            // receiving 3 2-byte (sensor_value_t which is uint16_t) values from uint8_t array which is gattc_evt.params.hvx.data
            glove_service_client_evt.params.sensor_data.value1 = 
                p_ble_evt->evt.gattc_evt.params.hvx.data[0] | p_ble_evt->evt.gattc_evt.params.hvx.data[1] << 8;
            glove_service_client_evt.params.sensor_data.value2 = 
                p_ble_evt->evt.gattc_evt.params.hvx.data[2] | p_ble_evt->evt.gattc_evt.params.hvx.data[3] << 8;
            glove_service_client_evt.params.sensor_data.value3 = 
                p_ble_evt->evt.gattc_evt.params.hvx.data[4] | p_ble_evt->evt.gattc_evt.params.hvx.data[5] << 8;

            p_glove_service_c->evt_handler(p_glove_service_c, &glove_service_client_evt);
        }
    }
}


/**@brief Function for handling Disconnected event received from the SoftDevice.
 *
 * @details This function check if the disconnect event is happening on the link
 *          associated with the current instance of the module, if so it will set its
 *          conn_handle to invalid.
 *
 * @param[in] p_glove_service_c Pointer to the Glove Client structure.
 * @param[in] p_ble_evt   Pointer to the BLE event received.
 */
static void on_disconnected(glove_service_client_t *p_glove_service_c, ble_evt_t const *p_ble_evt)
{
    if (p_glove_service_c->conn_handle == p_ble_evt->evt.gap_evt.conn_handle)
    {
        p_glove_service_c->conn_handle                              = BLE_CONN_HANDLE_INVALID;
        p_glove_service_c->peer_glove_service_db.sensor_cccd_handle = BLE_GATT_HANDLE_INVALID;
        p_glove_service_c->peer_glove_service_db.sensor_handle      = BLE_GATT_HANDLE_INVALID;
    }
}


void glove_service_on_db_disc_evt(glove_service_client_t *p_glove_service_c, ble_db_discovery_evt_t const *p_evt)
{
    // Check if the Glove Service was discovered.
    if (p_evt->evt_type == BLE_DB_DISCOVERY_COMPLETE &&
        p_evt->params.discovered_db.srv_uuid.uuid == GLOVE_SERVICE_UUID_SERVICE &&
        p_evt->params.discovered_db.srv_uuid.type == p_glove_service_c->uuid_type)
    {
        glove_service_client_evt_t evt;

        evt.evt_type    = BLE_GLOVE_SERVICE_CLIENT_EVT_DISCOVERY_COMPLETE;
        evt.conn_handle = p_evt->conn_handle;

        for (uint32_t i = 0; i < p_evt->params.discovered_db.char_count; i++)
        {
            const ble_gatt_db_char_t * p_char = &(p_evt->params.discovered_db.charateristics[i]);
            switch (p_char->characteristic.uuid.uuid)
            {
// ADD NEW CHARACTERSTIC HANDLERS HERE
//                case GLOVE_SERVICE_UUID_LED_CHAR:
//                    evt.params.peer_db.led_handle = p_char->characteristic.handle_value;
//                    break;
                case GLOVE_SERVICE_VALUE_CHAR_UUID:
                    evt.params.peer_db.sensor_handle      = p_char->characteristic.handle_value;
                    evt.params.peer_db.sensor_cccd_handle = p_char->cccd_handle;
                    break;

                default:
                    break;
            }
        }

        NRF_LOG_DEBUG("Glove Service discovered at peer.");
        //If the instance has been assigned prior to db_discovery, assign the db_handles
        if (p_glove_service_c->conn_handle != BLE_CONN_HANDLE_INVALID)
        {
            if (
                (p_glove_service_c->peer_glove_service_db.sensor_handle      == BLE_GATT_HANDLE_INVALID)&&
                (p_glove_service_c->peer_glove_service_db.sensor_cccd_handle == BLE_GATT_HANDLE_INVALID))
            {
                p_glove_service_c->peer_glove_service_db = evt.params.peer_db;
            }
        }

        p_glove_service_c->evt_handler(p_glove_service_c, &evt);

    }
}


uint32_t ble_glove_service_client_init(glove_service_client_t *p_glove_service_c, glove_service_client_init_t * p_glove_service_client_init)
{
    uint32_t      err_code;
    ble_uuid_t    glove_service_uuid;
    ble_uuid128_t glove_service_base_uuid = {GLOVE_SERVICE_UUID_BASE};

    VERIFY_PARAM_NOT_NULL(p_glove_service_c);
    VERIFY_PARAM_NOT_NULL(p_glove_service_client_init);
    VERIFY_PARAM_NOT_NULL(p_glove_service_client_init->evt_handler);

    p_glove_service_c->peer_glove_service_db.sensor_cccd_handle = BLE_GATT_HANDLE_INVALID;
    p_glove_service_c->peer_glove_service_db.sensor_handle      = BLE_GATT_HANDLE_INVALID;
    p_glove_service_c->conn_handle                              = BLE_CONN_HANDLE_INVALID;
    p_glove_service_c->evt_handler                              = p_glove_service_client_init->evt_handler;

    err_code = sd_ble_uuid_vs_add(&glove_service_base_uuid, &p_glove_service_c->uuid_type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    VERIFY_SUCCESS(err_code);

    glove_service_uuid.type = p_glove_service_c->uuid_type;
    glove_service_uuid.uuid = GLOVE_SERVICE_UUID_SERVICE;

    return ble_db_discovery_evt_register(&glove_service_uuid);
}

void glove_service_client_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    
    if ((p_context == NULL) || (p_ble_evt == NULL))
    {
        return;
    }

    glove_service_client_t * p_glove_service_c = (glove_service_client_t *)p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GATTC_EVT_HVX:
//            NRF_LOG_INFO("glove_service_client_on_ble_evt BLE_GATTC_EVT_HVX");
            on_hvx(p_glove_service_c, p_ble_evt);
            break;

        case BLE_GATTC_EVT_WRITE_RSP:
//            NRF_LOG_INFO("glove_service_client_on_ble_evt BLE_GATTC_EVT_WRITE_RSP");
            on_write_rsp(p_glove_service_c, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
//            NRF_LOG_INFO("glove_service_client_on_ble_evt BLE_GAP_EVT_DISCONNECTED");
            on_disconnected(p_glove_service_c, p_ble_evt);
            break;

        default:
            break;
    }
}


/**@brief Function for configuring the CCCD.
 *
 * @param[in] conn_handle The connection handle on which to configure the CCCD.
 * @param[in] handle_cccd The handle of the CCCD to be configured.
 * @param[in] enable      Whether to enable or disable the CCCD.
 *
 * @return NRF_SUCCESS if the CCCD configure was successfully sent to the peer.
 */
static uint32_t cccd_configure(uint16_t conn_handle, uint16_t handle_cccd, bool enable)
{
    NRF_LOG_INFO("Configuring CCCD. CCCD Handle = %d, Connection Handle = %d",
        handle_cccd,conn_handle);

    tx_message_t * p_msg;
    uint16_t       cccd_val = enable ? BLE_GATT_HVX_NOTIFICATION : 0;

    p_msg              = &m_tx_buffer[m_tx_insert_index++];
    m_tx_insert_index &= TX_BUFFER_MASK;

    p_msg->req.write_req.gattc_params.handle   = handle_cccd;
    p_msg->req.write_req.gattc_params.len      = WRITE_MESSAGE_LENGTH;
    p_msg->req.write_req.gattc_params.p_value  = p_msg->req.write_req.gattc_value;
    p_msg->req.write_req.gattc_params.offset   = 0;
    p_msg->req.write_req.gattc_params.write_op = BLE_GATT_OP_WRITE_REQ;
    p_msg->req.write_req.gattc_value[0]        = LSB_16(cccd_val);
    p_msg->req.write_req.gattc_value[1]        = MSB_16(cccd_val);
    p_msg->conn_handle                         = conn_handle;
    p_msg->type                                = WRITE_REQ;

    tx_buffer_process();
    return NRF_SUCCESS;
}


uint32_t glove_service_client_sensor_notif_enable(glove_service_client_t *p_glove_service_c)
{
    
    VERIFY_PARAM_NOT_NULL(p_glove_service_c);

    if (p_glove_service_c->conn_handle == BLE_CONN_HANDLE_INVALID)
    {
        return NRF_ERROR_INVALID_STATE;
    }

    return cccd_configure(p_glove_service_c->conn_handle,
                          p_glove_service_c->peer_glove_service_db.sensor_cccd_handle,
                          true);
}

uint32_t glove_service_client_handles_assign(glove_service_client_t    *p_glove_service_c,
                                  uint16_t         conn_handle,
                                  const glove_service_db_t *p_peer_handles)
{
    VERIFY_PARAM_NOT_NULL(p_glove_service_c);

    p_glove_service_c->conn_handle = conn_handle;
    if (p_peer_handles != NULL)
    {
        p_glove_service_c->peer_glove_service_db = *p_peer_handles;
    }
    return NRF_SUCCESS;
}


