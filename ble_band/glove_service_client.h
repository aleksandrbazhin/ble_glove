
/**@file
 *
 * @defgroup glove_service_c Glove Service Client
 * @{
 * @ingroup  ble_sdk_srv
 * @brief    The Glove Service client can be used to set a LED, and read a sensors state on a
 *           Glove service server.
 *
 * @details  This module contains the APIs and types exposed by the Glove Service Client
 *           module. These APIs and types can be used by the application to perform discovery of
 *           Glove Service at the peer and interact with it.
 *
 * @note    The application must register this module as BLE event observer using the
 *          NRF_SDH_BLE_OBSERVER macro. Example:
 *          @code
 *              glove_service_client_t instance;
 *              NRF_SDH_BLE_OBSERVER(anything, BLE_GLOVE_SERVICE_CLIENT_BLE_OBSERVER_PRIO,
 *                                   glove_service_client_on_ble_evt, &instance);
 *          @endcode
 */

#ifndef GLOVE_SERVICE_CLIENT
#define GLOVE_SERVICE_CLIENT

#define GLOVE_SERVICE_CLIENT_BLE_OBSERVER_PRIO 2

#include <stdint.h>
#include "ble.h"
#include "ble_db_discovery.h"
#include "nrf_sdh_ble.h"

/**@brief   Macro for defining a glove_service_c instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define GLOVE_SERVICE_CLIENT_CLIENT_DEF(_name)                                                           \
static glove_service_client_t _name;                                                                           \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     GLOVE_SERVICE_CLIENT_BLE_OBSERVER_PRIO,                                        \
                     glove_service_client_on_ble_evt,                                                     \
                     &_name)


#define GLOVE_SERVICE_UUID_BASE                 {0x4B, 0x69, 0xE7, 0xB4, 0xFA, 0xBB, 0xD2, 0xBC, \
                                                 0xE0, 0x4B, 0xAF, 0x65, 0x43, 0x0C, 0x4D, 0x15}
#define GLOVE_SERVICE_UUID_SERVICE               0x1366
#define GLOVE_SERVICE_VALUE_CHAR_UUID            0x1367

/**@brief Client event type. */
typedef enum
{
    BLE_GLOVE_SERVICE_CLIENT_EVT_DISCOVERY_COMPLETE = 1,  /**< Event indicating that the Glove Service has been discovered at the peer. */
    BLE_GLOVE_SERVICE_CLIENT_EVT_SENSOR_NOTIFICATION      /**< Event indicating that a notification of the Glove Button characteristic has been received from the peer. */
} glove_sevice_client_evt_type_t;

/**@brief Structure containing the Sensor value received from the peer. */
typedef uint16_t sensor_value_t;
typedef struct sensor_data_t
{
  sensor_value_t value1;
  sensor_value_t value2;
  sensor_value_t value3;
} sensor_data_t;


/**@brief Structure containing the handles related to the Glove Service found on the peer. */
typedef struct
{
    uint16_t sensor_cccd_handle;  /**< Handle of the CCCD of the Button characteristic. */
    uint16_t sensor_handle;       /**< Handle of the Sensor characteristic as provided by the SoftDevice. */
} glove_service_db_t;

/**@brief Glove Event structure. */
typedef struct
{
    glove_sevice_client_evt_type_t evt_type;    /**< Type of the event. */
    uint16_t                  conn_handle; /**< Connection handle on which the event occured.*/
    union
    {
        sensor_data_t      sensor_data;    /**< Sensor Value received. This will be filled if the evt_type is @ref BLE_GLOVE_SERVICE_CLIENT_EVT_BUTTON_NOTIFICATION. */
        glove_service_db_t peer_db;        /**< Glove Service related handles found on the peer device. This will be filled if the evt_type is @ref BLE_GLOVE_SERVICE_CLIENT_EVT_DISCOVERY_COMPLETE.*/
    } params;
} glove_service_client_evt_t;

// Forward declaration of the glove_service_client_t type.
typedef struct glove_service_client_s glove_service_client_t;

/**@brief   Event handler type.
 *
 * @details This is the type of the event handler that should be provided by the application
 *          of this module in order to receive events.
 */
typedef void (* glove_service_client_evt_handler_t) (glove_service_client_t * p_glove_service_c, glove_service_client_evt_t * p_evt);

/**@brief Glove Client structure. */
struct glove_service_client_s
{
    uint16_t                      conn_handle;  /**< Connection handle as provided by the SoftDevice. */
    glove_service_db_t            peer_glove_service_db;  /**< Handles related to Glove service on the peer*/
    glove_service_client_evt_handler_t evt_handler;  /**< Application event handler to be called when there is an event related to the Glove service. */
    uint8_t                       uuid_type;    /**< UUID type. */
};

/**@brief Glove Client initialization structure. */
typedef struct
{
    glove_service_client_evt_handler_t evt_handler;  /**< Event handler to be called by the Glove Client module whenever there is an event related to the Glove Service. */
} glove_service_client_init_t;


/**@brief Function for initializing the Glove client module.
 *
 * @details This function will register with the DB Discovery module. There it registers for the
 *          Glove Service. Doing so will make the DB Discovery module look for the presence
 *          of a Glove Service instance at the peer when a discovery is started.
 *
 * @param[in] p_glove_service_c      Pointer to the Glove client structure.
 * @param[in] p_glove_service_client_init Pointer to the Glove initialization structure containing the
 *                             initialization information.
 *
 * @retval    NRF_SUCCESS On successful initialization. Otherwise an error code. This function
 *                        propagates the error code returned by the Database Discovery module API
 *                        @ref ble_db_discovery_evt_register.
 */
uint32_t ble_glove_service_client_init(glove_service_client_t *p_glove_service_c, glove_service_client_init_t *p_glove_service_client_init);


/**@brief Function for handling BLE events from the SoftDevice.
 *
 * @details This function will handle the BLE events received from the SoftDevice. If a BLE event
 *          is relevant to the Glove Client module, then it uses it to update interval
 *          variables and, if necessary, send events to the application.
 *
 * @param[in] p_ble_evt     Pointer to the BLE event.
 * @param[in] p_context     Pointer to the Glove client structure.
 */
void glove_service_client_on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context);


/**@brief Function for requesting the peer to start sending notification of the Button
 *        Characteristic.
 *
 * @details This function will enable to notification of the Button at the peer
 *          by writing to the CCCD of the Button Characteristic.
 *
 * @param[in] p_glove_service_c Pointer to the Glove Client structure.
 *
 * @retval  NRF_SUCCESS If the SoftDevice has been requested to write to the CCCD of the peer.
 *                      Otherwise, an error code. This function propagates the error code returned
 *                      by the SoftDevice API @ref sd_ble_gattc_write.
 *          NRF_ERROR_INVALID_STATE if no connection handle has been assigned (@ref glove_service_client_handles_assign)
 *          NRF_ERROR_NULL if the given parameter is NULL
 */
uint32_t glove_service_client_sensor_notif_enable(glove_service_client_t *p_glove_service_c);


/**@brief Function for handling events from the database discovery module.
 *
 * @details Call this function when getting a callback event from the DB discovery module. This
 *          function will handle an event from the database discovery module, and determine if it
 *          relates to the discovery of Glove service at the peer. If so, it will call the
 *          application's event handler indicating that the Glove service has been discovered
 *          at the peer. It also populates the event with the service related information before
 *          providing it to the application.
 *
 * @param[in] p_glove_service_c Pointer to the Glove client structure.
 * @param[in] p_evt Pointer to the event received from the database discovery module.
 */
void glove_service_on_db_disc_evt(glove_service_client_t *p_glove_service_c, const ble_db_discovery_evt_t *p_evt);


/**@brief     Function for assigning a Handles to this instance of glove_service_c.
 *
 * @details Call this function when a link has been established with a peer to associate this link
 *          to this instance of the module. This makes it  possible to handle several links and
 *          associate each link to a particular instance of this module.
 *
 * @param[in] p_glove_service_c    Pointer to the Glove client structure instance to associate.
 * @param[in] conn_handle    Connection handle to associate with the given Glove Client Instance.
 * @param[in] p_peer_handles Glove Service handles found on the peer (from @ref BLE_GLOVE_SERVICE_CLIENT_EVT_DISCOVERY_COMPLETE event).
 *
 */
uint32_t glove_service_client_handles_assign(glove_service_client_t  *p_glove_service_c,
                                  uint16_t                 conn_handle,
                                  const glove_service_db_t *p_peer_handles);


/**@brief Function for writing the LED status to the connected server.
 *
 * @param[in] p_glove_service_c Pointer to the Glove client structure.
 * @param[in] status      LED status to send.
 *
 * @retval NRF_SUCCESS If the staus was sent successfully. Otherwise, an error code is returned.
 */
uint32_t glove_service_led_status_send(glove_service_client_t * p_glove_service_c, uint8_t status);


#endif 

/** @} */
