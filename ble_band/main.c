#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "nrf_pwr_mgmt.h"
#include "app_timer.h"
#include "app_pwm.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_db_discovery.h"
#include "glove_service_client.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_scan.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


#define SCAN_INTERVAL                   0x00A0                              /**< Determines scan interval in units of 0.625 millisecond. */
#define SCAN_WINDOW                     0x0050                              /**< Determines scan window in units of 0.625 millisecond. */
#define SCAN_DURATION                   0x0000                              /**< Timout when scanning. 0x0000 disables timeout. */

#define MIN_CONNECTION_INTERVAL         MSEC_TO_UNITS(7.5, UNIT_1_25_MS)    /**< Determines minimum connection interval in milliseconds. */
#define MAX_CONNECTION_INTERVAL         MSEC_TO_UNITS(30, UNIT_1_25_MS)     /**< Determines maximum connection interval in milliseconds. */
#define SLAVE_LATENCY                   0                                   /**< Determines slave latency in terms of connection events. */
#define SUPERVISION_TIMEOUT             MSEC_TO_UNITS(4000, UNIT_10_MS)     /**< Determines supervision time-out in units of 10 milliseconds. */

#define APP_BLE_CONN_CFG_TAG            1                                   /**< A tag identifying the SoftDevice BLE configuration. */
#define APP_BLE_OBSERVER_PRIO           3                                   /**< Application's BLE observer priority. You shouldn't need to modify this value. */


// define vars for motor-sensor interaction
#define SENSOR_VALUE_MAX                900   // maximum possible value for sensor when motor duty become maximum 1023 if resistors are equal
#define SENSOR_VALUE_TRIGGER_MIN        50    // minimum sensor value when motor triggers
#define MOTOR_DUTY_MAX                  100   // maximum pwm duty for motor
#define MOTOR_DUTY_TRIGGER_VALUE        10    // motor pwm when triggered

#define MOTOR_PIN_1                     14
#define MOTOR_PIN_2                     15
#define MOTOR_PIN_3                     16

NRF_BLE_SCAN_DEF(m_scan);                                       /**< Scanning module instance. */
GLOVE_SERVICE_CLIENT_CLIENT_DEF(m_glove_service_client);        /**< Main structure used by the Glove service client module. */
NRF_BLE_GATT_DEF(m_gatt);                                       /**< GATT module instance. */
BLE_DB_DISCOVERY_DEF(m_db_disc);                                /**< DB discovery module instance. */

APP_PWM_INSTANCE(PWM1, 1);     // Create the instance "PWM1" using TIMER1.
APP_PWM_INSTANCE(PWM2, 2);     // Create the instance "PWM2" using TIMER2.

typedef uint32_t motor_value_t;
static sensor_value_t const m_sensor_range = SENSOR_VALUE_MAX - SENSOR_VALUE_TRIGGER_MIN;
static motor_value_t const m_motor_range   = MOTOR_DUTY_MAX - MOTOR_DUTY_TRIGGER_VALUE;
static char const m_target_periph_name[] = "CSgl";              /**< Name of the device we try to connect to. This name is searched in the scan report data*/

static volatile bool ready_flag;            // A flag indicating PWM status.


void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    ready_flag = true;
}


/**@brief Function to handle asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num     Line number of the failing ASSERT call.
 * @param[in] p_file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(0xDEADBEEF, line_num, p_file_name);
}


/**@brief Function to start scanning.
 */
static void scan_start(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_scan_start(&m_scan);
    APP_ERROR_CHECK(err_code);

}


/**@brief Main function to convert data from sensor value to motor duty
 */
static motor_value_t convert_sensor_value(const sensor_value_t sensor_value) {
    if (sensor_value < SENSOR_VALUE_TRIGGER_MIN) {
        return 0;
    } else if (sensor_value >= SENSOR_VALUE_MAX) {
        return MOTOR_DUTY_MAX;
    } else {
        // like arduino map function
        return  MOTOR_DUTY_TRIGGER_VALUE + 
                (sensor_value - SENSOR_VALUE_TRIGGER_MIN) * m_motor_range / m_sensor_range;
    }
}


/**@brief Function to set motors pwn cycle
 */
static void set_motors_from_sensor_data(const sensor_data_t sensor_data) {
//    NRF_LOG_INFO("recieved sensor %d %d %d", 
//        sensor_data.value1,
//        sensor_data.value2,
//        sensor_data.value3);
    
    motor_value_t motor_value1 = convert_sensor_value(sensor_data.value1);
    motor_value_t motor_value2 = convert_sensor_value(sensor_data.value2);
    motor_value_t motor_value3 = convert_sensor_value(sensor_data.value3);
    
    APP_ERROR_CHECK(app_pwm_channel_duty_set(&PWM1, 0, motor_value1));
    APP_ERROR_CHECK(app_pwm_channel_duty_set(&PWM1, 1, motor_value2));
    APP_ERROR_CHECK(app_pwm_channel_duty_set(&PWM2, 0, motor_value3));

//    NRF_LOG_INFO("converted value %d %d %d", motor_value1, motor_value2, motor_value3);
}


/**@brief Handles events coming from the Glove central module.
 */
static void glove_service_client_evt_handler(glove_service_client_t *p_glove_service_c, glove_service_client_evt_t *p_glove_service_client_evt)
{
//    NRF_LOG_INFO("Glove service event %d", glove_service_client_evt->evt_type);
    switch (p_glove_service_client_evt->evt_type)
    {
        case BLE_GLOVE_SERVICE_CLIENT_EVT_DISCOVERY_COMPLETE:
        {
            ret_code_t err_code;

            err_code = glove_service_client_handles_assign(&m_glove_service_client,
                                                p_glove_service_client_evt->conn_handle,
                                                &p_glove_service_client_evt->params.peer_db);
//            NRF_LOG_INFO("Glove service discovered on conn_handle 0x%x.", p_glove_service_client_evt->conn_handle);

            // Glove service discovered. Enable notification of Sensor data.
            err_code = glove_service_client_sensor_notif_enable(p_glove_service_c);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GLOVE_SERVICE_CLIENT_EVT_SENSOR_NOTIFICATION:
        {

            set_motors_from_sensor_data(p_glove_service_client_evt->params.sensor_data);

        } break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code;

    // For readability.
    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

    switch (p_ble_evt->header.evt_id)
    {
        // Upon connection, check which peripheral has connected (HR or RSC), initiate DB
        // discovery and resume scanning if necessary. */
        case BLE_GAP_EVT_CONNECTED:
        {
            NRF_LOG_INFO("Connected.");
            err_code = glove_service_client_handles_assign(&m_glove_service_client, p_gap_evt->conn_handle, NULL);
            APP_ERROR_CHECK(err_code);

            err_code = ble_db_discovery_start(&m_db_disc, p_gap_evt->conn_handle);
            APP_ERROR_CHECK(err_code);

        } break;

        // Upon disconnection, reset the connection handle of the peer which disconnected and start scanning again.
        case BLE_GAP_EVT_DISCONNECTED:
        {
            NRF_LOG_INFO("Disconnected.");
            scan_start();
        } break;

        case BLE_GAP_EVT_TIMEOUT:
        {
            // We have not specified a timeout for scanning, so only connection attemps can timeout.
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                NRF_LOG_DEBUG("Connection request timed out.");
            }
        } break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
        {
            // Accept parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                        &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
        {
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTS_EVT_TIMEOUT:
        {
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
        } break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Glove client initialization.
 */
static void glove_serice_client_init(void)
{
    ret_code_t       err_code;
    glove_service_client_init_t glove_service_client_init_obj;

    glove_service_client_init_obj.evt_handler = glove_service_client_evt_handler;

    err_code = ble_glove_service_client_init(&m_glove_service_client, &glove_service_client_init_obj);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupts.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for handling Scaning events.
 *
 * @param[in]   p_scan_evt   Scanning event.
 */
static void scan_evt_handler(scan_evt_t const * p_scan_evt)
{
    ret_code_t err_code;
    //NRF_LOG_INFO("scan evt %d.", p_scan_evt->scan_evt_id);
    switch(p_scan_evt->scan_evt_id)
    {
        case NRF_BLE_SCAN_EVT_CONNECTING_ERROR:
            err_code = p_scan_evt->params.connecting_err.err_code;
            APP_ERROR_CHECK(err_code);
            break;
        case NRF_BLE_SCAN_EVT_NOT_FOUND:
//          NRF_LOG_INFO("scan evt not found");
          break;
        default:
          break;
    }
}


/**@brief Function for handling database discovery events.
 *
 * @details This function is callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function should forward the events
 *          to their respective services.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */
static void db_disc_handler(ble_db_discovery_evt_t * p_evt)
{
    glove_service_on_db_disc_evt(&m_glove_service_client, p_evt);
}


/**@brief Database discovery initialization.
 */
static void db_discovery_init(void)
{
    ret_code_t err_code = ble_db_discovery_init(db_disc_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the log.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing the timer.
 */
static void timer_init(void)
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Power manager. */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


static void scan_init(void)
{
    ret_code_t          err_code;
    nrf_ble_scan_init_t init_scan;

    memset(&init_scan, 0, sizeof(init_scan));

    init_scan.connect_if_match = true;
    init_scan.conn_cfg_tag     = APP_BLE_CONN_CFG_TAG;

    err_code = nrf_ble_scan_init(&m_scan, &init_scan, scan_evt_handler);
    APP_ERROR_CHECK(err_code);

    // Setting filters for scanning.
    err_code = nrf_ble_scan_filters_enable(&m_scan, NRF_BLE_SCAN_NAME_FILTER, false);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_NAME_FILTER, m_target_periph_name);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}


static void pwm_init(void) {

//    nrf_gpio_cfg_pin_output(MOTOR_PIN_1);
//    nrf_gpio_cfg_pin_output(MOTOR_PIN_2);
//    nrf_gpio_cfg_pin_output(MOTOR_PIN_3);

    /* 2-channel PWM, 200Hz, output on DK LED pins. */
    app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_2CH(5000L, MOTOR_PIN_1, MOTOR_PIN_2);
    app_pwm_config_t pwm2_cfg = APP_PWM_DEFAULT_CONFIG_1CH(5000L, MOTOR_PIN_3);
    /* Switch the polarity of channels. */
    pwm1_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;
    pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;
    pwm2_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;
    /* Initialize and enable PWM. */
    ret_code_t err_code;
    err_code = app_pwm_init(&PWM1, &pwm1_cfg, pwm_ready_callback);
    APP_ERROR_CHECK(err_code);
    app_pwm_enable(&PWM1);
    err_code = app_pwm_init(&PWM2, &pwm2_cfg, pwm_ready_callback);
    APP_ERROR_CHECK(err_code);
    app_pwm_enable(&PWM2);
}



/**@brief Function for handling the idle state (main loop).
 *
 * @details Handle any pending log operation(s), then sleep until the next event occurs.
 */
static void idle_state_handle(void)
{
    NRF_LOG_FLUSH();
    nrf_pwr_mgmt_run();
}




int main(void)
{
    // Initialize.
    log_init();
    timer_init();
    pwm_init();
    power_management_init();
    ble_stack_init();
    scan_init();
    gatt_init();
    db_discovery_init();
    glove_serice_client_init();

    // Start execution.
    NRF_LOG_INFO("CyberSense Glove band client started.");
    scan_start();

    // Enter main loop.
    for (;;)
    {
        idle_state_handle();
    }
}
