#ifndef BLE_GLOVE_UART_SERVICE_H__
#define BLE_BLE_GLOVE_UART_SERVICE_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"

// UUID 154d0c43-65af-4be0-bcd2-bbfab4e7694b 
#define GLOVE_SERVICE_UUID_BASE         {0x4B, 0x69, 0xE7, 0xB4, 0xFA, 0xBB, 0xD2, 0xBC, \
                                         0xE0, 0x4B, 0xAF, 0x65, 0x43, 0x0C, 0x4D, 0x15}
#define GLOVE_SERVICE_UUID               0x1366
#define GLOVE_VALUE_CHAR_UUID            0x1367


/**@brief   Macro for defining a ble_gus instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define BLE_GUS_DEF(_name)                                                                          \
static ble_gus_t _name;                                                                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_HRS_BLE_OBSERVER_PRIO,                                                     \
                     ble_gus_on_ble_evt, &_name)


typedef enum
{
    BLE_GUS_EVT_NOTIFICATION_ENABLED,
    BLE_GUS_EVT_NOTIFICATION_DISABLED,
    BLE_GUS_EVT_DISCONNECTED,
    BLE_GUS_EVT_CONNECTED
} ble_gus_evt_type_t;


/**@brief Custom Service event. */
typedef struct
{
    ble_gus_evt_type_t evt_type;                                  /**< Type of event. */
} ble_gus_evt_t;


// Forward declaration of the ble_gus_t type.
typedef struct ble_gus_s ble_gus_t;


/**@brief Custom Service event handler type. */
typedef void (*ble_gus_evt_handler_t) (ble_gus_t *p_gus, ble_gus_evt_t *p_evt);

typedef uint16_t sensor_value_t;
typedef struct sensor_data_type 
{
  sensor_value_t value1;
  sensor_value_t value2;
  sensor_value_t value3;
} sensor_data_type;

//typedef struct sensor_data_type 
//{
//  uint16 values[3];
//} sensor_data_type;


/**@brief Glove Service init structure. This contains all options and data needed for
 *        initialization of the service.*/
typedef struct
{
    ble_gus_evt_handler_t         evt_handler;
    sensor_data_type              init_sensor_data;           /**< Initial custom value */
    ble_srv_cccd_security_mode_t  custom_value_char_attr_md;     /**< Initial security level for Custom characteristics attribute */
} ble_gus_init_t;


/**@brief Glove UART Service structure. This contains various status information for the service. (forward declared before) */
struct ble_gus_s
{
    ble_gus_evt_handler_t         evt_handler;                    /**< Event handler to be called for handling events in the Custom Service. */
    uint16_t                      service_handle;                 /**< Handle of Custom Service (as provided by the BLE stack). */
    ble_gatts_char_handles_t      custom_value_handles;           /**< Handles related to the Custom Value characteristic. */
    uint16_t                      conn_handle;                    /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
    uint8_t                       uuid_type; 
};


/**@brief Function for initializing the Custom Service.
 *
 * @param[out]  p_cus       Custom Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_cus_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_gus_init(ble_gus_t *p_gus, const ble_gus_init_t *p_gus_init);


/**@brief Function for handling the Application's BLE Stack events.
 *
 * @details Handles all events from the BLE stack of interest to the Battery Service.
 *
 * @note 
 *
 * @param[in]   p_ble_evt  Event received from the BLE stack.
 * @param[in]   p_context  Custom Service structure.
 */
void ble_gus_on_ble_evt(ble_evt_t const *p_ble_evt, void *p_context);


/**@brief Function for updating the custom value.
 *
 * @details The application calls this function when the cutom value should be updated. If
 *          notification has been enabled, the custom value characteristic is sent to the client.
 *
 * @note 
 *       
 * @param[in]   p_cus          Custom Service structure.
 * @param[in]   Custom value 
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
uint32_t ble_gus_sensor_value_update(ble_gus_t *p_gus, sensor_data_type sensor_value);

#endif